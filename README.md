Birthday Greeting Kata
----------------------

# Problem: 

Write a program that:

- Loads a set of contact records from a CSV file or from a REST API
- Sends a greetings email to all contacts whose birthday is today
  - For testing purposes allow user to enter the month-day from a console (main)


# The goal

The goal of this exercise is to come up with a solution that is:

- Testable; we should be able to test the internal application logic with no need to ever send a real email.
- Flexible: 
  - we anticipate that the data source in the near future could change  to a relational database 
  - we also anticipate that the email service could soon be replaced with a service that sends greetings through other messaging platform
- Well-designed: separate clearly the business logic from the infrastructure.

# Tips

- Make it work. Make it right.

# Inspired by

- https://codingdojo.org/kata/birthday-greetings
- http://matteo.vaccari.name/blog/archives/154